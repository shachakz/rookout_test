import rook
import multiprocessing as mp
from threading import Thread
from time import sleep


def worker(_id: str):
    while True:
        print(f"Worker {_id} says hello!")
        sleep(1)


if __name__ == "__main__":
    rook.start(token='bc1d04d19e53efcd26aa5b0154e1594cba61ba90bbd6ff2586dfb5a18e69ca16')
    w1 = Thread(target=worker, kwargs={"_id": "w1"})
    w2 = Thread(target=worker, kwargs={"_id": "w2"})
    w1.start()
    w2.start()
    w1.join()
    w2.join()